#include "vmm_decoder.h"

#include "reply_type.h"

// std/stl
#include <iostream>
#include <sstream>
#include <stdio.h>
#include <iomanip>
using namespace std;

// boost

///////////////////////////////////////////////////////////////////////////////
VMMDecoder::VMMDecoder() :
    m_dbg(false)
{
    clear();
}
///////////////////////////////////////////////////////////////////////////////
void VMMDecoder::clear()
{
  
    m_tdo.resize(0);
    m_pdo.resize(0);
    m_chan.resize(0);
    m_vmm_id.resize(0);
    m_bcid_rel.resize(0);
    m_flag.resize(0);

    m_board_id.resize(0);
    m_bcid.resize(0);
    m_l1_id.resize(0);

    m_locked_elink.resize(0);

    m_raw_bcid = 0;
    m_raw_l1id = 0;

    m_raw_bcid = 0;
    m_raw_vmm_ID = 0;
    m_raw_chan_ID = 0;
    m_raw_pdo = 0;
    m_raw_tdo = 0;

    m_TTC_reg_address.resize(0);
    m_TTC_reg_value.resize(0);

}
uint32_t VMMDecoder::reverse_bits( uint32_t orig, int size){
  
  uint32_t reversed = 0;

  uint32_t bit_mask = 0b1;
  for ( int i=0; i<size; i++ ) {

    uint32_t tmp = ( orig & bit_mask ) >> i;
    reversed += ( tmp << ( size-1 - i ) );

    bit_mask = bit_mask << 1;
  }

  return reversed;
}

//----------------------------------------------------------------------//

unsigned int VMMDecoder::grayToBinary(unsigned int num)
{
  num = num ^ (num >> 16);
  num = num ^ (num >> 8);
  num = num ^ (num >> 4);
  num = num ^ (num >> 2);
  num = num ^ (num >> 1);
  return num;
}

/*
uint32_t VMMDecoder::grayToBinary(uint32_t gray_num)
{
  uint32_t mask;
  for (mask = gray_num >> 1; mask != 0; mask = mask >> 1)
    {
      gray_num = gray_num ^ mask;
    }
  return gray_num;

}
*/
//----------------------------------------------------------------------//
bool VMMDecoder::decode_reply(std::vector<uint32_t> datagram, uint32_t &udp_id, 
			      int &message_type, std::string *out_str, bool decode_data)
{

  stringstream m_sx;

  // different reply messages                                            
  //-------------------------------------------------------------------// 
  //   /header (16 bits)/UDP_ID (16 bits)/Module ID (8 bits)              
  //           /Message Type (8 bits)/0x0000 (16 bit place holder)
  //           /0x0 (1 bit place holder) + Data (31 bits)         
  //                   (perhaps multiple 32 bit place holder+data packets)
  //           /0xFFFFFFFF (32 bit trailer)                               
  //-------------------------------------------------------------------// 
  //   m_buffer is a string of hexadecimals                               
  //-------------------------------------------------------------------// 

  if ( datagram.size() < 3 ) {
    m_sx.str("");
    m_sx << "ERROR: reply size is too small. isize " << datagram.size() << " \n";
    *out_str = m_sx.str();
    return false;
  }

  //------------------------------------------------------------------//
  //                 Break Up Message into Parts                        
  //------------------------------------------------------------------//

  // first 32 bits 
  uint32_t header_hex = ( datagram.at(0) & 0xffff0000 ) >> 16;
  uint32_t UDP_ID_hex = ( datagram.at(0) & 0x0000ffff ) ;

  // second 32 bits
  uint32_t Mod_ID_hex       = ( datagram.at(1) & 0xff000000 ) >> 24;
  uint32_t Message_Type_hex = ( datagram.at(1) & 0x00ff0000 ) >> 16;
  uint32_t Message_ID_hex   = ( datagram.at(1) & 0x0000ff00 ) >> 8;
  //  uint32_t UnDef_hex        = ( datagram.at(1) & 0x000000ff ) ;

  uint32_t trailer_hex      = datagram.back();
  uint32_t next_to_last_hex  = 0;
  if ( datagram.size() >= 2 ) {
    next_to_last_hex = datagram.at( datagram.size()-2 ); // used to decode raw packages
  }

  //-----------------------------------------------------------------//
  //               Check Header + Trailer                              
  //-----------------------------------------------------------------//

  udp_id = UDP_ID_hex;

  //outputDatagram(datagram);

  if ( header_hex != 0xface ) {

    if ( ( next_to_last_hex & 0xfffffff0 ) == 0x77700ce0 ) {
      message_type = raw_data; 
      return true;
    }
    else {
      *out_str = "ERROR: reply message header is not 0xface \n";
      outputDatagram(header_hex);
      message_type = error_formatting;
      return false;
    }
  }

  if ( ( next_to_last_hex & 0xfffffff0 ) == 0x77700ce0 ) {
    message_type = raw_data;
    return true;
  }

  if ( trailer_hex != 0xffffffff )  {
    *out_str = "ERROR: reply message trailer is not 0xffffffff \n";
    outputDatagram(trailer_hex);
    message_type = error_formatting;
    return false;
  }

  //---------------------------------------------------------------//             
  //             Decode Module ID, Message Type                              
  //---------------------------------------------------------------//     

  //--------------------------------------------------------------//       
  //                          VMM data                                     
  //--------------------------------------------------------------//       

  if ( Mod_ID_hex == rep_modID_VMMout ) {
    message_type = data;

    // decode the data if needed
    if ( decode_data ) {
      m_tdo.resize(0);
      m_pdo.resize(0);
      m_chan.resize(0);
      m_vmm_id.resize(0);
      m_bcid_rel.resize(0);
      m_flag.resize(0);

      for ( uint i = 2; i < datagram.size()-1; i++ ) {
	decode_vmm_event_data(datagram.at(i));
      }

    }

    return true;
  }

  //---------------------------------------------------------------//
  //                  Ethernet receive module
  //---------------------------------------------------------------//

  if ( Mod_ID_hex == rep_modID_ethernet_rec ) {
    if      ( Message_Type_hex == rep_type_echo ) {
      message_type = ethernet_rec_echo;
      *out_str = "echo message \n";
      outputDatagram(datagram);
      return true;
    }
    else if ( Message_Type_hex == rep_type_err ) {
      message_type = ethernet_rec_err_unknown_cmd;
      if ( datagram.at(2) == 0x0bad0000 ) {
        *out_str = "Error: unknown command \n";
	outputDatagram(datagram);
	return false;
      }
      else if ( datagram.at(2) == 0x0bad0001) {
	message_type = ethernet_rec_err_FIFO_full;
        *out_str = "Error: Ethernet receiving FIFO full \n";
	outputDatagram(datagram);
	return false;
      }
      else if ( datagram.at(2) == 0x0bad0002) {
	message_type = ethernet_rec_err_target_timeout;
        *out_str = "Error: timeout waiting for target module \n";
	outputDatagram(datagram);
	return false;
      }
      else {
	message_type = ethernet_rec_err_undef;
	*out_str = "Error: Ethernet Receiver Unknown Error Type \n";
	outputDatagram(datagram);
	return false;
      }
    }
    else {
      message_type = ethernet_rec_err_undef;
      *out_str = "Error: Ethernet Receiver Unknown Message Type \n";
      outputDatagram(datagram);
      return false;
    }

  }

  //------------------------------------------------------------------//
  //                  Ethernet transmission module
  //------------------------------------------------------------------//

  if ( Mod_ID_hex == rep_modID_ethernet_trans ) {
    if ( Message_Type_hex == rep_type_err ) {
      if ( datagram.at(2) == 0x0bad0001 ) {
	message_type = ethernet_trans_err_send_FIFO_full;
	*out_str = "Error: Ethernet Transmission data sending FIFO full \n";
        outputDatagram(datagram);
        return false;
      }
      else if ( datagram.at(2) == 0x0bad0002 ) {
	message_type = ethernet_trans_err_length_FIFO_full;
	*out_str = "Error: Ethernet Transmission data length FIFO full \n";
        outputDatagram(datagram);
        return false;
      }
      else if ( datagram.at(2) == 0x0bad0003 ) {
	message_type = ethernet_trans_err_both_FIFO_full;
	*out_str = "Error: Ethernet Transmission data sending and length FIFO full \n";
        outputDatagram(datagram);
        return false;
      }
      else {
	message_type = ethernet_trans_err_undef;
	*out_str = "Error: unknown ethernet transmitter error \n";
	outputDatagram(datagram);
        return false;
      }
    }
    else {
      message_type = ethernet_trans_err_undef;
      *out_str = "Error: unknown ethernet transmitter message \n";
      outputDatagram(datagram);
      return false;
    }
  }

  //----------------------------------------------------------------//
  //                      SCA module
  //----------------------------------------------------------------//

  if ( Mod_ID_hex == rep_modID_SCA ) {
   
    uint32_t SCA_status = ( datagram.at(2) & 0xffff ); // lowest 16 bits
    
    if ( datagram.size() >= 3 ) m_current_SCA_ID = datagram.at(3); // current_SCA_ID is only valid after sca_init;

    if ( ( SCA_status & 0xffff ) == 0x0000 ) {
      message_type = SCA_normal;
      *out_str = "SCA status normal \n";
      return true;
    }
    else {

      message_type = SCA_error;
      
      if ( ( SCA_status & 0b1111000000000000 ) >> 12 == 0b1111 ) {
	*out_str = "SCA Error: SCA timeout error \n";
      }
      //      if ( ( SCA_status & 0b0011000000000000 ) >> 12 == 0b11 ) {
      //	*out_str = "SCA Error: SCA initialization error \n";
      //      }
      if ( ( SCA_status & 0b0000000100000000 ) >> 8 == 0b01 ) {
	*out_str = "SCA Error: SCA error \n";
      }
      if ( ( SCA_status & 0b0000001000000000 ) >> 8 == 0b10 ) {
	*out_str = "SCA Error: I2C error \n";
      }
      if ( ( SCA_status & 0b0000001100000000 ) >> 8 == 0b11 ) {
	*out_str = "SCA Error: Unrecognized ASIC type \n";
      }
      if ( ( SCA_status & 0b0000000011111111 ) == 0b11111111 ) {
	*out_str = "SCA Error: Rx Error Message  \n";
      }
      
      return false;
    }

  }
 
  //--------------------------------------------------------------//
  //                        TTC module
  //--------------------------------------------------------------//

  if ( Mod_ID_hex == rep_modID_TTC ) {
    if ( Message_Type_hex == rep_type_status ) {
      if ( Message_ID_hex == 0x00 ) {
        message_type = TTC_setting;
        *out_str = "decode TTC setting \n";
        outputDatagram(datagram);
	decode_current_TTC_setting( datagram );
        return true;
      }
      if ( Message_ID_hex == Elink_status_hex ) {
	message_type = TTC_query_elink;
	*out_str = "decode query elink status \n";
	outputDatagram(datagram);
	decode_query_elink( datagram.at(2) );
	return true;
      }
      else if ( Message_ID_hex == EVID_status_hex ) {
        message_type = TTC_query_EVID;
        *out_str = "decode query EVID status \n";
        outputDatagram(datagram);
        decode_query_EVID( datagram.at(2) );
        return true;
      }
      else if ( Message_ID_hex == TTC_status_hex ) {
        message_type = TTC_query_TTC;
        *out_str = "decode query TTC status \n";
        outputDatagram(datagram);
        decode_query_TTC_status( datagram.at(2) );
        return true;
      }
      else {
	message_type = TTC_undef;
	*out_str = "Error: Unknown TTC response \n";
	outputDatagram(datagram);
	return false;
      }
      if ( datagram.size() >= 3 ) m_TTC_firmware_version = datagram.at(3);
    }
    else {
      message_type = TTC_undef;
      *out_str = "Error: Unknown TTC response \n";
      outputDatagram(datagram);
      return false;
    }
  }

  message_type = error_formatting;
  *out_str = "Error: could not find response type \n";
  return false;

}

//----------------------------------------------------------------------//
void VMMDecoder::outputDatagram(std::vector<uint32_t> datagram)
{

  stringstream m_sx;

  m_sx << std::internal << std::setfill('0');

  for ( uint i=0; i < datagram.size(); i++ ) {
    m_sx << std::hex << std::setw(8) << datagram.at(i) ;
  }
  
  std::cout << "Output datagram: " << m_sx.str() << std::endl;

}
//----------------------------------------------------------------------//
void VMMDecoder::outputDatagram(uint32_t datagram)
{

  stringstream m_sx;

  m_sx << std::internal << std::setfill('0');

  m_sx << std::hex << std::setw(8) << datagram ;

  std::cout << " " << m_sx.str() << std::endl;

}

///////////////////////////////////////////////////////////////////////////////
bool VMMDecoder::decode_vmm_event_data(uint32_t d0)
{
    bool ok = true;

    //----------------------------------------------------//
    //    Read data using bit mask and bit shifting       //
    //----------------------------------------------------//

    // tdo is first 8 bits;
    m_tdo.push_back( d0 & 0xff );

    // pdo is next 10 bits (after 8 bits)
    m_pdo.push_back( ( d0 & 0x3ff00 ) >> 8 );

    // channel ID is next 6 bits ( after 18 bits )
    m_chan.push_back( ( d0 & 0xFC0000 ) >> 18 );

    // vmm ID is next 3 bits ( after 24 bits )
    m_vmm_id.push_back( ( d0 & 0x7000000 ) >> 24 );

    // bcid relative is next 3 bits ( after 27 bits )
    m_bcid_rel.push_back( (d0 & 0x38000000 ) >> 27 );

    // flag is next 2 bits ( after 30 bits )
    m_flag.push_back( ( d0 & 0xc0000000 ) >> 30 );

    return ok;
}
//-----------------------------------------------------------------------//
bool VMMDecoder::decode_vmm_raw_data(std::vector<uint32_t> datagram){

  if ( datagram.size() != 5 ) { //Example: FACE 0A9D 0BF2 0001 4200 A9DE DE00 0000 7770 0CE0 FFFF FFFF 
    std::cout << "warning: raw data event not of correct length" << std::endl;
    outputDatagram(datagram);
    return false;
  }

  //-----------------------------------//
  //  Decode the 4x8 bit header first  //
  //-----------------------------------//

  //uint32_t header     = datagram.at(0); //UDP DGRAM HEADER (8b) + UDP PACEKT ID (8b)
  uint32_t gray_bcid = ( datagram.at(1) & 0x0fff0000)>>16;
  //cout<<"gray_bcid"<<gray_bcid<<endl;
  gray_bcid           = reverse_bits( gray_bcid, 12);
  //cout<<"reverse_gray_bcid"<<gray_bcid<<endl;
  m_raw_bcid          = grayToBinary( gray_bcid );
  //cout<<"real_bcid "<<m_raw_bcid<<endl;
  m_raw_l1id          = (datagram.at(1) & 0x0000FFFF);

  //-----------------------------------//
  //  Decode the 4x8 bit vmm hit data  //
  //-----------------------------------//

  m_raw_rel_bcid      = ( datagram.at(2) & 0x38000000 ) >> 27;
  m_raw_vmm_ID        = ( datagram.at(2) & 0x07000000 ) >> 24;
  
  uint32_t raw_chan_ID_tmp   = ( datagram.at(2) & 0xFC0000 ) >> 18 ;
  m_raw_chan_ID       = reverse_bits( raw_chan_ID_tmp, 6);

  uint32_t pdo_lsb_first  = ( datagram.at(2) & 0x3FF00 ) >>8;
  m_raw_pdo           = reverse_bits( pdo_lsb_first, 10 );

  m_raw_tdo           = reverse_bits( datagram.at(2) & 0xFF, 8 );

  m_raw_elink         = ( datagram.at(3) & 0x0000000f );

  //---------------------------//
  // 	 Forget Trailer Bits   //
  //------ --------------------//

  return true;

}
//-----------------------------------------------------------------------//
bool VMMDecoder::decode_query_elink(uint32_t d0)
{
  bool ok = true;

  //----------------------------------------------------//                        
  //    Read data using bit mask and bit shifting       //                        
  //----------------------------------------------------//                        

  m_locked_elink.resize(0);

  uint32_t bit_mask = 0b1;

  for ( int i=0; i<8; i++ ) {
    if ( d0 & bit_mask ) m_locked_elink.push_back(true);
    else                 m_locked_elink.push_back(false);
    bit_mask = bit_mask << 1; // shift bit mask left by 1 (eg: 0001 to 0010)
  }

  return ok;
}

//-----------------------------------------------------------------------//
bool VMMDecoder::decode_query_EVID(uint32_t d0)
{
  bool ok = true;

  //----------------------------------------------------//                 
  //    Read data using bit mask and bit shifting       //                 
  //----------------------------------------------------//                 

  m_current_EVID = ( d0 & 0xffff ) ;

  return ok;
}


//-----------------------------------------------------------------------// 
bool VMMDecoder::decode_query_TTC_status(uint32_t d0)
{

  bool ok = true;

  m_current_TTC_status = ( d0 & 0xffff );

  return ok;
}

//-----------------------------------------------------------------------//
void VMMDecoder::decode_current_TTC_setting(std::vector<uint32_t> datagram ) {

  m_TTC_reg_address.resize(0);
  m_TTC_reg_value.resize(0);
  for ( uint i=2; i<datagram.size()-1; i=i+2 ) {
    m_TTC_reg_address.push_back( datagram.at(i) );
    m_TTC_reg_value.push_back( datagram.at(i+1) );
  }

}
