#include "board.h"

using namespace std;

Board::Board(int boardid)
{
    eventSize=0;
    board_id = boardid;

    stringstream m_sx;
    m_sx.str("");
    m_sx << "board_" << board_id;

    name = QString::fromStdString( m_sx.str() );


    QString t_ce = "h_channel_event "+name;
    QString t_pe = "h_pdo_event "+name;
    QString t_te = "h_tdo_event "+name;
    QString t_be = "h_bcid_event "+name;
    QString t_se = "h_size_event "+name;
    h_channel_eventScreen = new TH1D(t_ce.toUtf8().data(),t_ce.toUtf8().data(),64,0,64);
    h_pdo_eventScreen     = new TH1D(t_pe.toUtf8().data(),t_pe.toUtf8().data(),500,1,500);
    h_tdo_eventScreen     = new TH1D(t_te.toUtf8().data(),t_te.toUtf8().data(),500,1,500);
    h_bcid_eventScreen    = new TH1D(t_be.toUtf8().data(),t_be.toUtf8().data(),4096,0,4096);
    h_size_event          = new TH1D(t_se.toUtf8().data(),t_se.toUtf8().data(),10,0,10);

    QString t_cs = "Beam profile of "+name;
    QString t_ps = "PDO statistics of "+name;
    QString t_ts = "TDO statistics of "+name;
    QString t_bs = "BCID (Gray) statistics of "+name;
    QString t_ss = "Event size statistics of "+name;
    h_channel_statistics  = new TH2D(t_cs.toUtf8().data(),t_cs.toUtf8().data(),8,-0.5,7.5,64,0.0,64.0);
    h_pdo_statistics      = new TH1D(t_ps.toUtf8().data(),t_ps.toUtf8().data(),128,-0.5,1023.5);
    h_tdo_statistics      = new TH1D(t_ts.toUtf8().data(),t_ts.toUtf8().data(),128,-0.5,1023.5);
    h_bcid_statistics     = new TH1D(t_bs.toUtf8().data(),t_bs.toUtf8().data(),256,-0.5,4095.5);
    h_size_statistics     = new TH1D(t_ss.toUtf8().data(),t_ss.toUtf8().data(),10,0,10);

    h_channel_statistics->GetXaxis()->SetTitleSize(.5);
    h_pdo_statistics->GetXaxis()->SetTitleSize(.5);
    h_tdo_statistics->GetXaxis()->SetTitleSize(.5);
    h_bcid_statistics->GetXaxis()->SetTitleSize(.5);
    h_size_statistics->GetXaxis()->SetTitleSize(.5);

    h_channel_statistics->SetXTitle("VMM ID");
    h_channel_statistics->SetYTitle("VMM channel");
    h_channel_statistics->SetStats(kFALSE);

    h_channel_statistics->SetMinimum(0);

    //h_channel_statistics->SetFillStyle(3001);
    //h_pdo_statistics    ->SetFillStyle(3001);
    //h_tdo_statistics    ->SetFillStyle(3001);
    //h_bcid_statistics   ->SetFillStyle(3001);

    h_channel_statistics->SetFillColor(kRed-4);
    h_pdo_statistics    ->SetFillColor(kBlue-4);
    h_tdo_statistics    ->SetFillColor(kGreen-4);
    h_size_statistics    ->SetFillColor(kPink-4);
    //h_bcid_statistics   ->SetFillColor(kViolet);

}

int Board::getEventSize()
{
    return eventSize;
}
void Board::incrementEventSize()
{
    eventSize++;
}

void Board::resetEventSize()
{
    eventSize=0;
}

void Board::fillSizeEvent()
{
    h_size_event->Fill(eventSize);
}
void Board::fillSizeStatistics()
{
    h_size_statistics->Fill(eventSize);
}

void Board::resetAllHistos()
{
    h_channel_eventScreen->Reset();
    h_pdo_eventScreen->Reset();
    h_tdo_eventScreen->Reset();
    h_bcid_eventScreen->Reset();
    h_size_event->Reset();

    h_channel_statistics->Reset();
    h_pdo_statistics->Reset();
    h_tdo_statistics->Reset();
    h_bcid_statistics->Reset();
    h_size_statistics->Reset();
}

void Board::deleteAllHistos()
{

    h_channel_eventScreen->Delete();
    h_pdo_eventScreen->Delete();
    h_tdo_eventScreen->Delete();
    h_bcid_eventScreen->Delete();
    h_size_event->Delete();

    h_channel_statistics->Delete();
    h_pdo_statistics->Delete();
    h_tdo_statistics->Delete();
    h_bcid_statistics->Delete();
    h_size_statistics->Delete();
}

QString Board::getName()
{
    return name;
}

/*
Chamber* Board::getParent()
{
    return parent_chamber;
}
*/


int Board::getNoOfStatisticsHistos()
{
    return noOfStatisticsHistos;
}

TH2D *Board::getH_channel_statistics() const
{
    return h_channel_statistics;
}

TH1D *Board::getH_pdo_statistics() const
{
    return h_pdo_statistics;
}

TH1D *Board::getH_tdo_statistics() const
{
    return h_tdo_statistics;
}

TH1D *Board::getH_bcid_statistics() const
{
    return h_bcid_statistics;
}

TH1D *Board::getH_size_statistics() const
{
    return h_bcid_statistics;
}

TH1D *Board::getH_channel_eventScreen() const
{
    return h_channel_eventScreen;
}

TH1D *Board::getH_pdo_eventScreen() const
{
    return h_pdo_eventScreen;
}

TH1D *Board::getH_tdo_eventScreen() const
{
    return h_tdo_eventScreen;
}

TH1D *Board::getH_bcid_eventScreen() const
{
    return h_bcid_eventScreen;
}

TH1D *Board::getH_size_event() const
{
    return h_size_event;
}

void Board::drawChannelStatistics()
{
    h_channel_statistics->Draw("COLZ");
}

void Board::drawPdoStatistics()
{
    h_pdo_statistics->Draw();
}

void Board::drawTdoStatistics()
{
    h_tdo_statistics->Draw();
}

void Board::drawBCIDStatistics()
{
    h_bcid_statistics->Draw();
}

void Board::drawSizeStatistics()
{
    h_size_statistics->Draw();
}
void Board::drawChannelEvent()
{
    h_channel_eventScreen->Draw();
}

void Board::drawPdoEvent()
{
    h_pdo_eventScreen->Draw();
}

void Board::drawTdoEvent()
{
    h_tdo_eventScreen->Draw();
}

void Board::drawBCIDEvent()
{
    h_bcid_eventScreen->Draw();
}

void Board::drawSizeEvent()
{
    h_size_event->Draw();
}
