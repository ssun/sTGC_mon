#ifndef BOARD_H
#define BOARD_H

#include <QString>
#include <TH2D.h>
#include <TH1D.h>
//#include "chamber.h"

//C++
#include <fstream>
#include <sstream>
#include <string>
#include <iostream>
#include <vector>

#include <cstdint>
#include <stdint.h>


class Board
{
public:
  Board(int);
  //    Chamber* getParent();

  int get_vmm_id() { return vmm_id; };
  int get_board_id() { return board_id; };
  QString getName();
  void drawChannelStatistics();
  void drawPdoStatistics();
  void drawTdoStatistics();
  void drawBCIDStatistics();
  void drawSizeStatistics();
  void drawChannelEvent();
  void drawPdoEvent();
  void drawTdoEvent();
  void drawBCIDEvent();
  void drawSizeEvent();
  
  static int getNoOfStatisticsHistos();
  
  TH2D *getH_channel_statistics() const;
  TH1D *getH_pdo_statistics() const;
  TH1D *getH_tdo_statistics() const;
  TH1D *getH_bcid_statistics() const;
  TH1D *getH_size_statistics() const;
  
  TH1D *getH_channel_eventScreen() const;
  TH1D *getH_pdo_eventScreen() const;
  TH1D *getH_tdo_eventScreen() const;
  TH1D *getH_bcid_eventScreen() const;
  TH1D *getH_size_event() const;
  
  void fillSizeEvent();
  void fillSizeStatistics();
  void incrementEventSize();
  int getEventSize();
  void resetEventSize();
  
  void resetAllHistos();
  void deleteAllHistos();
  
 private:
    //    Chamber *parent_chamber;
    int vmm_id;
    int board_id;

    TH1D* h_channel_eventScreen;
    TH1D* h_pdo_eventScreen;
    TH1D* h_tdo_eventScreen;
    TH1D* h_bcid_eventScreen;
    TH1D* h_size_event;

    TH2D* h_channel_statistics;
    TH1D* h_pdo_statistics;
    TH1D* h_tdo_statistics;
    TH1D* h_bcid_statistics;
    TH1D* h_size_statistics;
    QString name;
    static int const noOfStatisticsHistos=4;
    int eventSize;
};

#endif // BOARD_H
