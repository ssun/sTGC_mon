#include "mainwindow.h"
#include "ui_mainwindow.h"

using namespace std;

MainWindow::MainWindow(QWidget *parent) :
    debugMode(0),
    isPaused(0),
    isConfigured(0),
    m_socket_receiver(0),
    eventCount(0),
    QMainWindow(parent),
    file_last_read_loc(0),
    nMaxEventsRead(100),
    previous_evid(0),
    last_evid(0),
    previous_LB_evid(0),
    nLB(0),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(ui->setupTreeWidget,SIGNAL(itemSelectionChanged()),this,SLOT(treeSelectionChanged()));

    createCanvas();
    ui->tabWidget->setCurrentIndex(0);

    ui->combo_refreshNevt->addItem("50 hits/ref");
    ui->combo_refreshNevt->addItem("100 hits/ref");
    ui->combo_refreshNevt->addItem("200 hits/ref");
    ui->combo_refreshNevt->addItem("500 hits/ref");
    ui->combo_refreshNevt->setCurrentIndex(1);

    ui->combo_refreshRate->addItem("0.1 sec");
    ui->combo_refreshRate->addItem("0.5 sec");
    ui->combo_refreshRate->addItem("1 sec");
    ui->combo_refreshRate->addItem("3 sec");
    ui->combo_refreshRate->setCurrentIndex(2);

    ui->current_run_bt->setChecked(true);

    decoder = new VMMDecoder();

    nevt_vs_time = new vector<int>;
    nevt_vs_time->resize(0);

    // ------------------------------------------------------------------------------- //

    initHistos();
    setupCanvas();

    /*
    nMaxEvents = 5000;
    setInputBinary("run_PHYSICS_063_raw.bin");
    RunInputBinary();
    */

    // ------------------------------------------------------------------------------- //

    lumiblock_update_timer = new QTimer();
    connect(lumiblock_update_timer, SIGNAL(timeout()), this, SLOT(updateLB()));

    vmmC_update_timer = new QTimer();
    connect(vmmC_update_timer, SIGNAL(timeout()), this, SLOT(updateVMM()));

    boardC_update_timer = new QTimer();
    connect(boardC_update_timer, SIGNAL(timeout()), this, SLOT(updateBoard()));

    overviewC_update_timer = new QTimer();
    connect(overviewC_update_timer, SIGNAL(timeout()), this, SLOT(updateOverview()));

    // ------------------------------------------------------------------------------- //

    connect(ui->view_board_id, SIGNAL(currentIndexChanged(int)),
            this, SLOT(viewBoard()));
    connect(ui->view_vmm_id, SIGNAL(currentIndexChanged(int)),
            this, SLOT(viewVMM()));

    connect(ui->b_Reset, SIGNAL(clicked()), this, SLOT(on_b_Reset_released()));

    connect(ui->current_run_bt, SIGNAL(clicked()), this, SLOT( run_type_changed() ) );
    connect(ui->finished_run_bt, SIGNAL(clicked()), this, SLOT( run_type_changed() ) );

    // ------------------------------------------------------------------------------- //

    /*
    startCanvasUpdates();
    treeSelectionChanged();//trigger this to setup correctly                                   

    ui->l_configd->setText("<font color='green'>Is configured!</font>");

    isConfigured = true;
    */

    // -------------------------------------------------------------------------------- //

    connect(ui->outputFile_config_dir,     SIGNAL(clicked()),
            this, SLOT(selectOutputDir()));

    connect(ui->BeginMon,     SIGNAL(clicked()),
            this, SLOT(startMon()));

    connect(ui->StopMon,     SIGNAL(clicked()),
            this, SLOT(stopMon()));


    //drawAllChips();

}

MainWindow::~MainWindow() {
  fclose(ptr_myfile);
}

// ------------------------------------------------------------------------------------ //

void MainWindow::PopUpError(QString str, QString info) {
  msgBox.setText(str);
  if ( info != "" ) msgBox.setInformativeText(info);
  msgBox.setIcon(QMessageBox::Critical);
  msgBox.exec();
}

void MainWindow::PopUpError(const char* str, const char *info) {
  PopUpError( QString(str),
              QString(info) );
}

// ------------------------------------------------------------------------------------ //
bool MainWindow::setupMetaFile() {
  QDir ftest(ui->outputDirField->text());

  if ( !ftest.exists() ) {
    PopUpError( "run directory does not exist" );
    return false;
  }

  QStringList filters;
  filters << "*_meta.txt";
  ftest.setNameFilters(filters);

  QStringList fileName = ftest.entryList(QDir::Files, QDir::Time);

  if ( fileName.size() == 0 ) {
    //    PopUpWarning( "no run meta data file" );
    return false;
  }

  std::cout << ui->outputDirField->text().toStdString() << "/" << fileName.at(0).toStdString() << std::endl;

  std::string input_file_meta_str = ( ui->outputDirField->text().toStdString() + "/" + fileName.at(0).toStdString() );

  if ( input_file_meta.is_open() ) {
    input_file_meta.close();
  }

  input_file_meta.open(input_file_meta_str.c_str());
  if ( input_file_meta.is_open() ) return true;
  else return false;

}

void MainWindow::getRunStartTime() {

  std::string line;

  if ( input_file_meta.is_open() ) {
    input_file_meta.seekg(0,ios::beg);
    if ( getline ( input_file_meta,line ) ) {
      ui->run_start_time->setText( QString::fromStdString(line) );
    }
  }
}

void MainWindow::monitorEndRun() {
  std::string line;
  if ( input_file_meta.is_open() ) {
    if ( getline ( input_file_meta,line ) ) {
      ui->run_end_time->setText( QString::fromStdString(line) );
      if ( ui->current_run_bt->isChecked() ) stopMon();
    }
    input_file_meta.close();
  }
  return;
}

void MainWindow::startMon() {

  QDir ftest(ui->outputDirField->text());

  if ( !ftest.exists() ) {
    PopUpError( "run directory does not exist" );
    return;
  }

  //--------------------------------------------------------------//
  //     Search for raw.bin file in run directory
  //--------------------------------------------------------------//

  QStringList filters;
  filters << "*_raw.bin";
  ftest.setNameFilters(filters);

  QStringList fileName = ftest.entryList(QDir::Files, QDir::Time);

  if ( fileName.size() == 0 ) {
    PopUpError( "no run binary data file" );
    return;
  }

  std::cout << ui->outputDirField->text().toStdString() << "/" << fileName.at(0).toStdString() << std::endl;

  input_file_binary = ( ui->outputDirField->text().toStdString() + "/" + fileName.at(0).toStdString() );

  if ( ptr_myfile == NULL ) {
    fclose(ptr_myfile);
  }

  ptr_myfile=fopen(input_file_binary.c_str(),"rb");
  if (!ptr_myfile) {
    PopUpError("Unable to open file!");
    return;
  }

  //-----------------------------------------------------------------------//
  
  setupMetaFile();
  getRunStartTime();
  if ( ui->finished_run_bt->isChecked() ) monitorEndRun();

  //-----------------------------------------------------------------------//

  ui->l_configd->setText("<font color='green'>Is Monitoring!</font>");

  resetAllHistos();
  //startCanvasUpdates();
  treeSelectionChanged();//trigger this to setup correctly                                                                                                                     
  ui->current_run_bt->setEnabled(false);
  ui->finished_run_bt->setEnabled(false);

  isConfigured = true;

  //-----------------------------------------------------------------------//
  
  viewVMM();
  viewBoard();
  viewOverview();

}
void MainWindow::stopMon() {

  stopCanvasUpdates();
  ui->mon_end_time->setText(GetCurrentTimeStr());

  ui->current_run_bt->setEnabled(true);
  ui->finished_run_bt->setEnabled(true);

  ui->l_configd->setText("<font color='red'>NOT Monitoring!</font>");

}
// ------------------------------------------------------------------------------------ //
void MainWindow::selectOutputDir()
{

  QString filename = QFileDialog::getExistingDirectory(this,
                                                       tr("Load output directory"), "../output/" );

  if(filename.isNull()) return;

  QDir ftest(filename);

  if(!ftest.exists()) {

    ui->outputDirField->setText("");

    ui->outputDirField->setStyleSheet("QLineEdit { background: rgb(220, 0, 0); selection-background-color: rgb(233, 99, 0); }");
    ui->outputDirField->setStyleSheet("QLineEdit { background: white; selection-background-color: rgb(233, 99, 0); }");
  }
  else {
    ui->outputDirField->setText(filename);
  }

}

// --------------------------------------------------------------------------------------
void MainWindow::run_type_changed() {
  if ( QObject::sender() == ui->current_run_bt ) {
    ui->current_run_bt->setChecked(true);
    ui->finished_run_bt->setChecked(false);
  }
  if ( QObject::sender() == ui->finished_run_bt ) {
    ui->current_run_bt->setChecked(false);
    ui->finished_run_bt->setChecked(true);
  }

}

QString MainWindow::GetCurrentTimeStr() {

  QTime time = QTime::currentTime();

  int day, month, year;
  QDate date = QDate::currentDate();
  date.getDate(&year,&month,&day);

  QString timestr = time.toString("hh:mm");

  stringstream m_sx;
  m_sx.str("");
  m_sx << day << "/" << month << "/" << year << " ";

  QString datestr = QString::fromStdString(m_sx.str());

  return datestr+timestr;

}

/// open file SETUP ---------------------------------------------------------------------
void MainWindow::RunInputBinary() {
  
  std::stringstream m_sx;
  //  m_sx << std::internal << std::setfill('0');

  string line;

  uint32_t buffer_uint;

  vector<uint32_t> datagram;
  datagram.resize(0);

  ievent = 0;

  ptr_myfile=fopen(input_file_binary.c_str(),"rb");
  if (!ptr_myfile) {
    printf("Unable to open file!");
    return;
  }

}
// --------------------------------------------------------------------------------
void MainWindow::Decode_and_Fill( std::vector<uint32_t> datagram, bool is_first_evt ) {

  //  std::cout << "decoding and filling " << ievent << std::endl;

  uint32_t udp_id;
  int message_type;
  std::string out_str;
  bool output_decoded = false;


  bool verbose = false;

  bool reply_normal = decoder->decode_reply(datagram, udp_id, message_type,
                                            &out_str, output_decoded);

  if ( !reply_normal ) return;

  if ( message_type == raw_data ) {
    decoder->decode_vmm_raw_data(datagram);

    if ( verbose ) std::cout << "UPD_ID " << udp_id ;
    if ( verbose ) std::cout << " EVID " << decoder->raw_l1id() ;
    if ( verbose ) std::cout << " elink ID " << decoder->raw_elink();
    if ( verbose ) std::cout << " vmm ID " << decoder->raw_vmm_ID() ;
    if ( verbose ) std::cout << " chan ID " << decoder->raw_chan_ID() ;
    if ( verbose ) std::cout << " pdo " << decoder->raw_pdo() ;
    if ( verbose ) std::cout << " tdo " << decoder->raw_tdo() ;
    if ( verbose ) std::cout << " bcid " << decoder->raw_bcid() ;
    if ( verbose ) std::cout << " rel_bcid " << decoder->raw_rel_bcid() << std::endl;

    m_ip = "";

    m_bcid = decoder->raw_bcid();
    m_l1_id = decoder->raw_l1id();

    m_bcid_rel = decoder->raw_rel_bcid();
    m_vmm_id   = decoder->raw_vmm_ID();
    m_chan     = decoder->raw_chan_ID();
    m_pdo      = decoder->raw_pdo();
    m_tdo      = decoder->raw_tdo();

    m_elink_id = decoder->raw_elink();

    if ( ievent == 0 ) {
      first_evt_evid = m_l1_id;
      previous_LB_evid = m_l1_id;
    }

    previous_evid = last_evid;
    last_evid = GetCurrentEVID(m_l1_id);

    //    std::cout << " l1_id " << m_l1_id << " last evid " << last_evid 
    //	      << " previous_evid " << previous_evid << " previous_LB " << previous_LB_evid << std::endl;

    ievent++;

    //    m_flag     = 0;

    //if ( !is_first_evt ) {
    fillVMM(m_l1_id, m_vmm_id, m_elink_id, m_chan, m_pdo, m_tdo, m_bcid);
    fillBoard(m_l1_id, m_vmm_id, m_elink_id, m_chan, m_pdo, m_tdo, m_bcid);
    //}
  }
}
// --------------------------------------------------------------------------------
void MainWindow::updateData_finishedRun() {
  
  //  std::stringstream m_sx;

  uint32_t buffer_uint;

  vector<uint32_t> datagram;
  datagram.resize(0);

  long nMaxEvent = ievent + 10000;

  while ( fread(&buffer_uint,sizeof(buffer_uint),1,ptr_myfile) && ievent < nMaxEvent )  {

    datagram.push_back( buffer_uint );
    if ( buffer_uint == 0xffffffff ) {
      Decode_and_Fill(datagram);
      datagram.resize(0);
    }

  }

}

void MainWindow::updateData() {

  //  std::stringstream m_sx;

  uint32_t buffer_uint;

  vector<uint32_t> datagram;
  datagram.resize(0);

  uint32_t data_size = 5;

  //--------------------------------------------------------------------------------------------------//   
  //      if it is the first event to be monitored then read in last event and set everything              
  //--------------------------------------------------------------------------------------------------//   
  if ( ievent == 0 ) {

    //     last location is current end of file                                                            
    fseek( ptr_myfile,0,SEEK_END);
    file_last_read_loc = ftell( ptr_myfile );

    //----------------------------------------------------------//                                         
    //   check if there is even enough data for one event                                                  
    //----------------------------------------------------------//                                         
    if ( file_last_read_loc < sizeof(buffer_uint)*data_size ) {
      return; // give up wait for the next time there is data                                              
    }

    //----------------------------------------------------------//                                         
    //    there exist at least one event, read it in   
    //----------------------------------------------------------//                                         
    else {

      nMaxEventsRead = 100;

      switch (ui->combo_refreshNevt->currentIndex()) {
      case 0:
	nMaxEventsRead = 50;
	break;
      case 1:
	nMaxEventsRead = 100;
	break;
      case 2:
	nMaxEventsRead = 200;
	break;
      case 3:
	nMaxEventsRead = 500;
	break;
      default:
	nMaxEventsRead = 100;
	break;
      }

      if ( file_last_read_loc <= -nMaxEventsRead*sizeof(buffer_uint)*data_size ) {
	fseek( ptr_myfile,-file_last_read_loc,SEEK_END);
      }
      else {
	fseek( ptr_myfile,-nMaxEventsRead*sizeof(buffer_uint)*data_size,SEEK_END);
      }

      //-------------------------------------------------------//

      while ( fread(&buffer_uint,sizeof(buffer_uint),1,ptr_myfile) && ievent < nMaxEventsRead)  {

	//stringstream m_sx;
	//m_sx.str("");
	//m_sx << std::hex << std::setw(8) << buffer_uint ;
	//std::cout << "first evt " << m_sx.str() << std::endl;    

	datagram.push_back( buffer_uint );
	if ( buffer_uint == 0xffffffff ) {
	  Decode_and_Fill(datagram, true);
	  datagram.resize(0);
	}

      }

      fseek( ptr_myfile,0,SEEK_END);
      file_last_read_loc = ftell( ptr_myfile ); 

      return;
    }
  }


  //-----------------------------------------------------------------------------------//
  //              Read in data if not the first event to be monitored
  //-----------------------------------------------------------------------------------//

  if ( ievent != 0 ) {

    //  find location of end of file
    fseek( ptr_myfile,0,SEEK_END);
    long current_loc = ftell( ptr_myfile );
    
    //  if the end of file is the same as last time, stop, no more data has been read since last time

    //std::cout << "current loc " << current_loc << " file_last_read_loc " << file_last_read_loc << std::endl;
    if ( current_loc == file_last_read_loc ) {
      //std::cout << "ievent " << ievent << std::endl;
      //std::cout << "no new data events" << std::endl;
      return; 
    }
    
    int readEvents;
    
    //--------------------------------------------------------------------------
    //   max number of events to be read per check
    //   if number of new events is less then max events, read it all out
    //--------------------------------------------------------------------------
    
    nMaxEventsRead = 100;

    switch (ui->combo_refreshNevt->currentIndex()) {
    case 0:
      nMaxEventsRead = 50;
      break;
    case 1:
      nMaxEventsRead = 100;
      break;
    case 2:
      nMaxEventsRead = 200;
      break;
    case 3:
      nMaxEventsRead = 500;
      break;
    default:
      nMaxEventsRead = 100;
      break;
    }
    
    if ( ( current_loc - file_last_read_loc )*data_size <= nMaxEventsRead ) {
      readEvents = (current_loc - file_last_read_loc)/data_size;
    }
    else {
      readEvents = nMaxEventsRead;
    }

    fseek( ptr_myfile,(sizeof(buffer_uint)*data_size*readEvents*-1),SEEK_END);
    
    //-------------------------------------------------------
    //                 read till the end
    //-------------------------------------------------------
    
    while ( fread(&buffer_uint,sizeof(buffer_uint),1,ptr_myfile) )  {
      stringstream m_sx;
      m_sx.str("");
      m_sx << std::hex << std::setw(8) << buffer_uint ;
      //std::cout << m_sx.str() << std::endl;
      
      datagram.push_back( buffer_uint );
      if ( buffer_uint == 0xffffffff ) {
	Decode_and_Fill(datagram);
	datagram.resize(0);
      }
      
    }

    fseek( ptr_myfile,0,SEEK_END);
    file_last_read_loc = ftell( ptr_myfile ); // set last place as 

  }


}
unsigned long MainWindow::GetCurrentEVID( uint32_t l1id ) {

  unsigned long i_times = ( previous_evid + first_evt_evid ) / 0xffff ;
  
  if ( l1id+1 < ( previous_evid + first_evt_evid ) % 0xffff ) {
    i_times++;
  }

  return ( l1id + i_times * 0xffff ) - first_evt_evid;

}

void MainWindow::updateLB() {
  if ( ui->finished_run_bt->isChecked() ) return;

  int nevt_collected = last_evid - previous_LB_evid;
  nLB++;

  if ( ievent !=0 ) nevt_vs_time->push_back( nevt_collected );


  if ( nLB > 2 ) {
    h_event_time->SetBins(nLB-2,-0.5,nLB-2-0.5);
    for ( int i=1; i<nevt_vs_time->size(); i++ ) {
      h_event_time->SetBinContent(i,nevt_vs_time->at(i));
    }
  }
  else {
    h_event_time->SetBins(1,-0.5,0.5);
    h_event_time->SetBinContent(1,0);
  }

  //c_overview->cd(1);
  c_time->cd(1);
  h_event_time->Draw();
  c_time->ModAndUpd_Pads();

  previous_LB_evid = last_evid;

  monitorEndRun();
}

/// CANVAS SETUP ------------------------------------------------------------------
void MainWindow::createCanvas()
{

  //just initializing 4 canvases (time, vmm, board, and overview display)    

  c_time = new QMainCanvas();
  c_time->resize(c_time->sizeHint());

  auto layout4 = new QGridLayout();
  layout4->setHorizontalSpacing(0);
  layout4->setVerticalSpacing(0);
  layout4->setContentsMargins(0,0,0,0);
  layout4->addWidget(c_time);
  ui->timeFrame->setLayout(layout4);
  //c_time->resize(ui->timeFrame->size());

  //-----------------------------------------------------//
    c_vmm = new QMainCanvas();
    c_vmm->resize(c_vmm->sizeHint());
    //    c_vmm->setWindowTitle("vmm-mon Statistics");
    //    c_vmm->setGeometry( 100, 100, 700, 500 );
    //    c_vmm->show();

    auto layout1 = new QGridLayout();
    layout1->setHorizontalSpacing(0);
    layout1->setVerticalSpacing(0);
    layout1->setContentsMargins(0,0,0,0);
    layout1->addWidget(c_vmm);
    ui->vmmFrame->setLayout(layout1);

    c_board = new QMainCanvas();
    c_board->resize(c_board->sizeHint());
    //    c_board->setWindowTitle("vmm-mon Event Display");
    //    c_board->setGeometry( 150, 150, 700, 500 );
    //    c_board->show();

    auto layout2 = new QGridLayout();
    layout2->setHorizontalSpacing(0);
    layout2->setVerticalSpacing(0);
    layout2->setContentsMargins(0,0,0,0);
    layout2->addWidget(c_board);
    ui->boardFrame->setLayout(layout2);

    c_overview = new QMainCanvas();
    c_overview->resize(c_overview->sizeHint());
    //    c_vmm->setWindowTitle("vmm-mon Statistics");
    //    c_vmm->setGeometry( 100, 100, 700, 500 );   
    //    c_vmm->show();                              

    auto layout3 = new QGridLayout();
    layout3->setHorizontalSpacing(0);
    layout3->setVerticalSpacing(0);
    layout3->setContentsMargins(0,0,0,0);
    layout3->addWidget(c_overview);
    ui->overviewFrame->setLayout(layout3);


}
void MainWindow::setupCanvas()
{
    ///Divide dimensions are init'd here, and adjusted in treeSelectionChanged()
    //the canvas will have a line for every chip,board,or chamber
    //from the selected ones
    //    canvas_size_in_y = chips.size();
    //this is probably 4 for chips,boards, or chambers...to be seen
    //    canvas_size_in_x = Chip::getNoOfStatisticsHistos();

    //the histograms are created in the Chip class
    //since each of them refers to a chip
    //no functionality yet for histograms per chamber

    //let's make one canvas to rule them all
    //this canvas will be defined by what is selected in the gui
    //each line can be a chip, a board, a chamber

    //chip   :         hitmap,pdo,tdo,bcid + eventDisplay on itself
    //board  :combined hitmap,pdo,tdo,bcid + eventDisplay on itself
    //chamber:combined hitmap,pdo,tdo,bcid + eventDisplay on itself
    //so, each item=line will have 4 histos (dual, with the eventDisplay)

    calculateCanvasSizeX();
    c_time->Divide(1,1);
    c_vmm->Divide(canvas_size_in_x,1);
    c_board->Divide(1,1);
    c_overview->Divide(2,4);

    viewVMM();
    viewBoard();
    viewOverview();
    updateLB();
}
void MainWindow::initHistos()
{

  h_event_time = new TH1D("h_event_time", "nTriggers vs Lumi-Block", 1,-0.5,0.5);
  h_event_time->SetXTitle("Lumi-Block (1 min)");
  h_event_time->SetYTitle("nTriggers per Lumi-Block");
  h_event_time->SetStats(kFALSE);     

  for ( int i=0; i<8; i++ ) { // loop over 8 boards

    Board* b = new Board(i);
    boards.push_back(b);

    std::vector<Chip*> temp_chip_vec;
    temp_chip_vec.resize(0);

    for ( int j=0;j<8; j++ ) { // loop over 8 vmm per board // for sFEB
      Chip* c = new Chip(j,i,0); 
      temp_chip_vec.push_back(c);
    }
    
    chips.push_back(temp_chip_vec);
  }

}
void MainWindow::fillVMM(int trig_cnt, int vmm_id, int board_id, int strip, int pdo,int tdo, int bcid)
{
    fill_counter++;

    Chip* c = chips.at( board_id ).at( vmm_id );

    c->fillSizeStatistics();

    c->getH_channel_statistics()->Fill(strip);
    c->getH_pdo_statistics()->Fill(pdo);
    c->getH_tdo_statistics()->Fill(tdo);
    c->getH_bcid_statistics()->Fill(bcid);

    fillBoard( trig_cnt, vmm_id, board_id, strip, pdo, tdo, bcid );
}
void MainWindow::fillBoard(int trig_cnt, int vmm_id, int board_id, int strip, int pdo,int tdo, int bcid)
{

  Board* b = boards.at( board_id );

  b->fillSizeStatistics();

  b->getH_channel_statistics()->Fill(vmm_id,strip);
  b->getH_pdo_statistics()->Fill(pdo);
  b->getH_tdo_statistics()->Fill(tdo);
  b->getH_bcid_statistics()->Fill(bcid);
}


// ------------------------------------------------------------------
void MainWindow::viewVMM()
{

  int iboard = ui->view_board_id->currentIndex();
  int ivmm   = ui->view_vmm_id->currentIndex();

  int temp_cd=1;
  Chip *tempChip = chips.at(iboard).at(ivmm);
  Board *tempBoard = boards.at(iboard);

  if ( ui->cb_showHit->isChecked() ) {
    c_vmm->cd(temp_cd);
    tempChip->drawChannelStatistics();
    temp_cd++;
  }

  if(ui->cb_showPDO->isChecked()) {
    c_vmm->cd(temp_cd);
    tempChip->drawPdoStatistics();
    temp_cd++;
  }

  if(ui->cb_showTDO->isChecked()) {
    c_vmm->cd(temp_cd);
    tempChip->drawTdoStatistics();
    temp_cd++;
  }

  if(ui->cb_showBCID->isChecked()) {
    c_vmm->cd(temp_cd);
    tempChip->drawBCIDStatistics();
    temp_cd++;
  }

  //if(ui->cb_showEventSize->isChecked()) {
    //c_vmm->cd(temp_cd);
    //tempChip->drawSizeStatistics();
    //temp_cd++;
  //}

  vmmC_updatePads();
}

void MainWindow::viewBoard()
{

  int iboard = ui->view_board_id->currentIndex();
  int ivmm   = ui->view_vmm_id->currentIndex();

  int temp_cd=1;
  Chip *tempChip = chips.at(iboard).at(ivmm);
  Board *tempBoard = boards.at(iboard);

  if ( ui->cb_showHit->isChecked() ) {
    c_board->cd(temp_cd);
    tempBoard->drawChannelStatistics();
    temp_cd++;
  }

  boardC_updatePads();

}

void MainWindow::viewOverview() {

  for ( int i=0; i < boards.size(); i++ ) {
    c_overview->cd(i+1);
    boards.at(i)->drawChannelStatistics();
  }

  overviewC_updatePads();

}

/// DRAWING FUNCTIONS ------------------------------------------------
void MainWindow::drawAllChips()
{
    debug(__FUNCTION__);
    int temp_cd=1;
    for(int i=0; i<chips.size(); i++)
    {

      for(int j=0; j<chips.at(i).size(); j++) {

	Chip *tempChip = chips.at(i).at(j);

        if(ui->cb_showHit->isChecked())
        {
            //            qDebug()<< "plotting hits "<<ui->cb_showHit->isChecked();
            c_vmm->cd(temp_cd);
            tempChip->drawChannelStatistics();
            c_board->cd(temp_cd);
            tempChip->drawChannelEvent();
            temp_cd++;
        }

        if(ui->cb_showPDO->isChecked())
        {
            c_vmm->cd(temp_cd);
            tempChip->drawPdoStatistics();
            c_board->cd(temp_cd);
            tempChip->drawPdoEvent();
            temp_cd++;
        }

        if(ui->cb_showTDO->isChecked())
        {
            c_vmm->cd(temp_cd);
            tempChip->drawTdoStatistics();
            c_board->cd(temp_cd);
            tempChip->drawTdoEvent();
            temp_cd++;
        }

        if(ui->cb_showBCID->isChecked())
        {
            c_vmm->cd(temp_cd);
            tempChip->drawBCIDStatistics();
            c_board->cd(temp_cd);
            tempChip->drawBCIDEvent();
            temp_cd++;
        }

        if(ui->cb_showEventSize->isChecked())
        {
            c_vmm->cd(temp_cd);
            tempChip->drawSizeStatistics();
            c_board->cd(temp_cd);
            tempChip->drawSizeEvent();
            temp_cd++;
        }
      }
    }
}
void MainWindow::resetAllHistos()
{

  previous_evid = 0;
  last_evid = 0;
  previous_LB_evid = 0;

  ievent = 0;

  nevt_vs_time->resize(0);
  nLB = 0;

  h_event_time->SetBins(1,-0.5,0.5);
  h_event_time->SetMinimum(0);

  for( int i=0; i<chips.size(); i++ ) {
    for ( int j=0; j<chips.at(i).size(); j++ ) {
      Chip* c = chips.at(i).at(j);
      c->resetAllHistos();
    }
  }
  for ( int i=0; i<boards.size(); i++ ) {
    boards.at(i)->resetAllHistos();
  }

  //---------------------------------------------------//

  ui->mon_start_time->setText(GetCurrentTimeStr());
  ui->mon_end_time->setText("");

  /*
  switch (ui->tabWidget->currentIndex()) {
  case 0:
    updateOverview();
    break;
  case 1:
    updateBoard();
    break;
  case 2:
    updateVMM();
    break;
  default:
    break;
  }
  */
  updateLB();
  viewVMM();
  viewBoard();
  viewOverview();

  //----------------------//

  previous_evid = 0;
  last_evid = 0;
  previous_LB_evid = 0;

  ievent = 0;

  nevt_vs_time->resize(0);

  nLB = 0;

}
void MainWindow::deleteAllHistos()
{
  for( int i=0; i<chips.size(); i++ ) {
    for( int j=0; j<chips.at(i).size(); j++ ) {
      Chip* c =chips.at(i).at(j);
      c->deleteAllHistos();
    }
  }

}
// ----------------------------------------------------------------------------------
void MainWindow::updateVMM() {
  if ( ui->current_run_bt->isChecked() ) updateData();
  else                                   updateData_finishedRun();
  viewVMM();
}
void MainWindow::updateBoard() {
  if ( ui->current_run_bt->isChecked() ) updateData();
  else                                   updateData_finishedRun();
  viewBoard();
}
void MainWindow::updateOverview() {
  if ( ui->current_run_bt->isChecked() ) updateData();
  else                                   updateData_finishedRun();
  viewOverview();
}
/// CANVAS UPDATE CONTROL -----------------------------------------------------------
void MainWindow::startCanvasUpdates()
{
    //    update_timer = new QTimer();
    //    connect(update_timer, SIGNAL(timeout()), this, SLOT(updatePads()));
    //    update_timer->start(1000);
    //---------------------------------------

  // 1 lumiblock = 1 min
  lumiblock_update_timer->start(60000);

  switch (ui->tabWidget->currentIndex()) {
  case 0:
    overviewC_update_timer->start(refreshRate);
    boardC_update_timer->stop();
      vmmC_update_timer->stop();
      break;
  case 1:
    overviewC_update_timer->stop();
    boardC_update_timer->start(refreshRate);
    vmmC_update_timer->stop();
    break;
  case 2:
    overviewC_update_timer->stop();
    boardC_update_timer->stop();
    vmmC_update_timer->start(refreshRate);
    break;
  default:
    break;
  }

}
void MainWindow::stopCanvasUpdates()
{
    //    update_timer->stop();

  lumiblock_update_timer->stop();
  vmmC_update_timer->stop();
  boardC_update_timer->stop();
  overviewC_update_timer->stop();
}
void MainWindow::updatePads()
{
    c_vmm->ModAndUpd_Pads();
    c_board->ModAndUpd_Pads();
    c_overview->ModAndUpd_Pads();
}

void MainWindow::vmmC_updatePads()
{
    c_vmm->ModAndUpd_Pads();
}
void MainWindow::boardC_updatePads()
{
  c_board->ModAndUpd_Pads();
}
void MainWindow::overviewC_updatePads()
{
  c_overview->ModAndUpd_Pads();
}

/// TOOLS ---------------------------------------------------------------------------
void MainWindow::calculateCanvasSizeX()
{
    canvas_size_in_x=0;
    if(ui->cb_showHit->isChecked()) canvas_size_in_x++;
    if(ui->cb_showPDO->isChecked()) canvas_size_in_x++;
    if(ui->cb_showTDO->isChecked() ) canvas_size_in_x++;
    if(ui->cb_showBCID->isChecked() ) canvas_size_in_x++;
    //    if(ui->cb_showEventSize->isChecked() ) canvas_size_in_x++;
    //    qDebug() << "canvas_size_in_x="<<canvas_size_in_x;
}
Chip* MainWindow::findChip(int vmm_id, int board_id)
{
  Chip *c = chips.at(board_id).at(vmm_id);
  return c;
}
void MainWindow::printInfo()
{
    qDebug() << "------------------";
    for (int i=0; i<chips.size();i++) {
      for(int j=0;j<chips.at(i).size();j++)
      {
	Chip* cc=chips.at(i).at(j);
	qDebug()<<cc->getName();
      }
      qDebug() << "------------------";
    }
}
void MainWindow::debug(QString s)
{
    if(debugMode)
        qDebug() << s;
}
/// UI CONTROL ----------------------------------------------------------------------
void MainWindow::treeSelectionChanged()
{
    ///stop the updates
    stopCanvasUpdates();
    c_time->clear();
    c_vmm->clear();
    c_board->clear();
    c_overview->clear();

    //selected items list
    QList<QTreeWidgetItem *> list = ui->setupTreeWidget->selectedItems();

    int n_vmms = chips.at(ui->view_board_id->currentIndex()).size();

    canvas_size_in_y = n_vmms;

    //redo the setup
    setupCanvas();
    ///restart updates
    startCanvasUpdates();

    //updateLB();
    viewVMM();
    viewBoard();
    viewOverview();
}
void MainWindow::on_b_Reset_released()
{
    resetAllHistos();
}
void MainWindow::on_b_Pause_released()
{
    if(isPaused)
    {
        isPaused = false;
	startCanvasUpdates();
	ui->l_configd->setText("<font color='green'>Is Monitoring!</font>");
        ui->b_Pause->setText("Pause");
    }
    else
    {
        isPaused = true;
	stopCanvasUpdates();
	ui->l_configd->setText("<font color='red'>Is Paused!</font>");
        ui->b_Pause->setText("Resume");
    }

}
void MainWindow::on_b_clearTreeSelection_released()
{
    ui->setupTreeWidget->clearSelection();
}

void MainWindow::on_cb_showHit_released()
{
    if(!isConfigured)
        return;

    stopCanvasUpdates();
    c_vmm->clear();
    c_board->clear();
    c_overview->clear();

    setupCanvas();
    startCanvasUpdates();
}
void MainWindow::on_cb_showPDO_released()
{
    if(!isConfigured)
        return;

    stopCanvasUpdates();
    c_vmm->clear();
    c_board->clear();
    c_overview->clear();

    setupCanvas();
    startCanvasUpdates();
}
void MainWindow::on_cb_showTDO_released()
{
    if(!isConfigured)
        return;
    stopCanvasUpdates();
    c_vmm->clear();
    c_board->clear();
    c_overview->clear();

    setupCanvas();
    startCanvasUpdates();
}
void MainWindow::on_cb_showBCID_released()
{
    if(!isConfigured)
        return;
    stopCanvasUpdates();
    c_vmm->clear();
    c_board->clear();
    c_overview->clear();

    setupCanvas();
    startCanvasUpdates();
}
void MainWindow::on_cb_showEventSize_released()
{
    if(!isConfigured)
        return;

    stopCanvasUpdates();
    c_vmm->clear();
    c_board->clear();
    c_overview->clear();

    setupCanvas();
    startCanvasUpdates();
}

void MainWindow::on_combo_refreshRate_currentIndexChanged(int index)
{

    if(!isConfigured)
        return;

    stopCanvasUpdates();
    switch (index) {
    case 0:
        refreshRate = 100;
        break;
    case 1:
        refreshRate = 500;
        break;
    case 2:
        refreshRate = 1000;
        break;
    case 3:
        refreshRate = 3000;
        break;
    default:
        break;
    }
    startCanvasUpdates();
}

void MainWindow::on_b_exportPng_released()
{
    png_index++;
    c_time->SaveAs("TriggerRate"+QString::number(png_index)+".png");
    c_vmm->SaveAs("vmm"+QString::number(png_index)+".png");
    c_board->SaveAs("Board"+QString::number(png_index)+".png");
    c_overview->SaveAs("Overview"+QString::number(png_index)+".png");

}

void MainWindow::on_tabWidget_currentChanged(int index)
{
    if(isConfigured)
        startCanvasUpdates();
}
